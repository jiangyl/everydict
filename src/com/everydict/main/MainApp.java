package com.everydict.main;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public class MainApp extends Application {
	
	private WindowManager.LayoutParams wmParams=new WindowManager.LayoutParams();

	public WindowManager.LayoutParams getMywmParams(){
		wmParams.type=LayoutParams.TYPE_PHONE;  
        wmParams.format=PixelFormat.RGBA_8888;  

        wmParams.flags=LayoutParams.FLAG_NOT_TOUCH_MODAL
                              | LayoutParams.FLAG_NOT_FOCUSABLE;
        
        wmParams.x=0;
        wmParams.y=0;
        
        SharedPreferences triggerData = getSharedPreferences("trigger_data", 0);
        wmParams.gravity=triggerData.getInt("GRAVITY", Gravity.RIGHT|Gravity.BOTTOM);   
        wmParams.width=triggerData.getInt("width", 50);
        wmParams.height=triggerData.getInt("height", 200);
		return wmParams;
	}
}
