package com.everydict.main;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.TextView;

public class TranFloatView extends TextView {
    public static final String SEARCH_ACTION   = "colordict.intent.action.SEARCH";
    public static final String EXTRA_QUERY    = "EXTRA_QUERY";
    public static final String EXTRA_FULLSCREEN = "EXTRA_FULLSCREEN";
    public static final String EXTRA_HEIGHT   = "EXTRA_HEIGHT";
    public static final String EXTRA_WIDTH    = "EXTRA_WIDTH";
    public static final String EXTRA_GRAVITY   = "EXTRA_GRAVITY";
    public static final String EXTRA_MARGIN_LEFT  = "EXTRA_MARGIN_LEFT";
    public static final String EXTRA_MARGIN_TOP   = "EXTRA_MARGIN_TOP";
    public static final String EXTRA_MARGIN_BOTTOM  = "EXTRA_MARGIN_BOTTOM";
    public static final String EXTRA_MARGIN_RIGHT  = "EXTRA_MARGIN_RIGHT";
    
	public TranFloatView(Context context) {
		super(context);		
		// TODO Auto-generated constructor stub
	}
	
	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
		 //call colordict app 
		 Intent intent = new Intent(SEARCH_ACTION);
		 Context mycontext = super.getContext();
		 SharedPreferences triggerData = mycontext.getSharedPreferences("trigger_data", 0);
		 if (isIntentAvailable(mycontext,intent)){
			 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 intent.putExtra(EXTRA_FULLSCREEN, false); 
			 intent.putExtra(EXTRA_HEIGHT, 400); 
			 intent.putExtra(EXTRA_GRAVITY,triggerData.getInt("EXTRA_GRAVITY",Gravity.TOP));
			 mycontext.startActivity(intent);
		 }else{
			 intent = new Intent(Intent.ACTION_VIEW);
			 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 intent.setData(Uri.parse("market://search?q=pname:com.socialnmobile.colordict"));
			 mycontext.startActivity(intent);
		 }
		 
		 return super.onTouchEvent(event);
		}
	 
	 public static boolean isIntentAvailable(Context context, Intent intent) {
		 final PackageManager packageManager = context.getPackageManager();
		 List list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		 return list.size() > 0;  
		}
	 
}
