package com.everydict.main;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.content.SharedPreferences;
import android.widget.RadioButton;  
import android.widget.RadioGroup;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
	
	private WindowManager wm=null;
	private WindowManager.LayoutParams wmParams=null;
	private TranFloatView myTranView=null;
    
	private SeekBar triggerWidth,triggerHeight;
	private RadioGroup positionRadioGroup1,dict_position;
	private RadioButton potition_left,position_right,position_top,position_bottom;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createView();
        initSeekBar();
        initRadioButton();
        initDict();
    }
    
    private void createView(){
    	myTranView = new TranFloatView(getApplicationContext());
    	myTranView.setBackgroundColor(android.graphics.Color.YELLOW);
    	
    	wm=(WindowManager)getApplicationContext().getSystemService("window");
    	wmParams = ((MainApp)getApplication()).getMywmParams();
        wm.addView(myTranView, wmParams);
    }
    
    //init the height and width of the trigger area 
    private void initSeekBar(){
    	triggerWidth = (SeekBar) this.findViewById(R.id.trigger_width);
        triggerHeight = (SeekBar) this.findViewById(R.id.trigger_height);
        
        triggerWidth.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar){
            	wm.removeView(myTranView);
            	wm.addView(myTranView, wmParams);
            }
            
            public void onStartTrackingTouch(SeekBar seekBar){
            }
            
            public void onProgressChanged(SeekBar seekBar,int progress,boolean fromUser){
            	wmParams.width=progress; 
            	SharedPreferences triggerData = getSharedPreferences("trigger_data", 0);
            	triggerData.edit().putInt("width",progress).commit();
            }
        });
        
        triggerHeight.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar){
            	wm.removeView(myTranView);
            	wm.addView(myTranView, wmParams);
            }
            
            public void onStartTrackingTouch(SeekBar seekBar){
            }
            
            public void onProgressChanged(SeekBar seekBar,int progress,boolean fromUser){
            	wmParams.height=progress;
            	SharedPreferences triggerData = getSharedPreferences("trigger_data", 0);
            	triggerData.edit().putInt("height", progress).commit();
            }
        });
    }
    
    private void initRadioButton(){
    	SharedPreferences triggerData = getSharedPreferences("trigger_data", 0);
        potition_left = (RadioButton) findViewById(R.id.position_left);
        position_right = (RadioButton) findViewById(R.id.position_right);
        if ( triggerData.getInt("LEFT",1) == 1 ){
        	potition_left.setChecked(true) ;
        }else{
        	position_right.setChecked(true) ;
        }
        
    	positionRadioGroup1 = (RadioGroup) findViewById(R.id.position1);
    	positionRadioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { 
    		SharedPreferences triggerData = getSharedPreferences("trigger_data", 0);
    		
    		@Override
    		public void onCheckedChanged(RadioGroup group,int checkId){
    		    if(checkId == potition_left.getId() ){
    		    	wmParams.gravity=Gravity.BOTTOM|Gravity.LEFT;
    		    	triggerData.edit().putInt("GRAVITY",wmParams.gravity).commit();
    		    	triggerData.edit().putInt("LEFT",1).commit();	
    		    }else{
    		    	wmParams.gravity=Gravity.BOTTOM|Gravity.RIGHT;  
    		    	triggerData.edit().putInt("GRAVITY",wmParams.gravity).commit();
    		    	triggerData.edit().putInt("LEFT",0).commit();	
    		    }
    		    wm.removeView(myTranView);
            	wm.addView(myTranView, wmParams);
    		}
    	});
    	
    }

    private void initDict(){
    	SharedPreferences triggerData = getSharedPreferences("trigger_data", 0);
    	position_top = (RadioButton) findViewById(R.id.position_top);
        position_bottom = (RadioButton) findViewById(R.id.position_bottom);
        if ( triggerData.getInt("TOP",1) == 1 ){
        	position_top.setChecked(true) ;
        }else{
        	position_bottom.setChecked(true) ;
        }
        
        dict_position = (RadioGroup) findViewById(R.id.dict_position);
        dict_position.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { 
    		SharedPreferences triggerData = getSharedPreferences("trigger_data", 0);
    		
    		@Override
    		public void onCheckedChanged(RadioGroup group,int checkId){
    		    if(checkId == position_top.getId() ){
    		    	triggerData.edit().putInt("TOP",1).commit();
    		    	triggerData.edit().putInt("EXTRA_GRAVITY",Gravity.TOP).commit();
    		    }else{
    		    	triggerData.edit().putInt("TOP",0).commit();
    		    	triggerData.edit().putInt("EXTRA_GRAVITY",Gravity.BOTTOM).commit();
    		    }
    		}
    	});
    	
    }
    
    @Override
    public void onRestart(){
    	super.onRestart();
    	myTranView.setBackgroundColor(android.graphics.Color.YELLOW);
    }
    
    @Override
    public void onPause(){
    	super.onPause();
    }
    
    @Override
    public void onStop(){
    	super.onStop();
    	myTranView.setBackgroundColor(Color.TRANSPARENT);
    }
    
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	wm.removeView(myTranView);
    }
    
}
